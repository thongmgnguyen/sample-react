import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { ConnectedRouter} from 'react-router-redux'
import './App.css';
import ToDoContainer from './containers/ToDoContainer';
import ProjectSettingContainer from './containers/ProjectSettingContainer';

class App extends Component {

  render() {
    return (
        <Switch>
          <Route path="/project-settings" component={ProjectSettingContainer} />
          <Route component={ToDoContainer}/>
        </Switch>
    );
  }
}

export default App;
