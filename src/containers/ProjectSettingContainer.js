import { connect } from 'react-redux';
import  ProjectSetting from './../components/ProjectSetting';

const mapStateToProps = (state) => {
  return {
    projectId: state.todos.summary
  }
}

const mapDispatchToProps = dispatch => {
}

const ProjectSettingContainer = connect(
  mapStateToProps
)(ProjectSetting);

export default ProjectSettingContainer;

