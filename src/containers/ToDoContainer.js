import { connect } from 'react-redux'
import TodoList from './../components/TodoList';


const mapStateToProps = state => {
  return {
    summary: state.todos.summary
  }
}

const ToDoContainer = connect(
  mapStateToProps
)(TodoList);

export default ToDoContainer;