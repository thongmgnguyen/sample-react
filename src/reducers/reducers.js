import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import todos from './todos';


const todoApp = combineReducers({
  todos: todos,
  router: routerReducer
});

export default todoApp;